-- --------------------------------------------------------
-- Hostitel:                     192.168.1.112
-- Verze serveru:                5.6.24 - MySQL Community Server (GPL)
-- OS serveru:                   Linux
-- HeidiSQL Verze:               9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportování struktury pro tabulka BasketTest.BasketItems
CREATE TABLE IF NOT EXISTS `BasketItems` (
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `count` tinyint(4) NOT NULL,
  PRIMARY KEY (`userId`,`productId`),
  KEY `FK_ProductBasketItems` (`productId`),
  CONSTRAINT `FK_BasketUserBasketItems` FOREIGN KEY (`userId`) REFERENCES `BasketUser` (`id`),
  CONSTRAINT `FK_ProductBasketItems` FOREIGN KEY (`productId`) REFERENCES `Product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování struktury pro tabulka BasketTest.BasketUser
CREATE TABLE IF NOT EXISTS `BasketUser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `lastName` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- Exportování struktury pro tabulka BasketTest.Product
CREATE TABLE IF NOT EXISTS `Product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku BasketTest.Product: ~11 rows (přibližně)
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` (`id`, `name`, `price`) VALUES
	(1, 'Samsung Galaxy S2', 5000),
	(2, 'Samsung Galaxy S3', 8000),
	(3, 'Samsung Galaxy S4', 10000),
	(4, 'Samsung Galaxy S5', 13000),
	(5, 'Samsung Galaxy S6', 17000),
	(6, 'Samsung Galaxy S6 Edge', 21000),
	(7, 'Samsung Galaxy Note', 7000),
	(8, 'Samsung Galaxy Note 2', 9500),
	(9, 'Samsung Galaxy Note 3', 11000),
	(10, 'Samsung Galaxy Note 4', 17000),
	(11, 'Samsung Galaxy Note 5', 23000);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
