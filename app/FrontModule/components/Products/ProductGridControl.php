<?php

namespace App\FrontModule\Control;

use App;

class ProductGridControl extends App\Components\BaseControl
{

    /** @var App\FrontModule\Model\ProductRepository */
    private $productRep;

    /** @var App\FrontModule\Control\Factory\IAddToBasketControl */
    public $componentsAddToBasket;

    public function __construct(App\FrontModule\Model\ProductRepository $productRep, App\FrontModule\Control\Factory\IAddToBasketControl $basketAdd)
    {
        parent::__construct();
        $this->productRep = $productRep;
        $this->componentsAddToBasket = $basketAdd;
    }

    protected function createTemplate()
    {
        $template = parent::createTemplate();
        $template->products = $this->productRep->getAll();
        return $template;
    }

}
