<?php

namespace App\FrontModule\Control\Factory;

use App;

interface IProductGridControl
{

    /** @return App\FrontModule\Control\ProductGridControl */
    function create();
}
