<?php

namespace App\FrontModule\Control;

use App;

class BasketStatusControl extends App\Components\BaseControl
{

    /** @var App\FrontModule\Model\BasketService */
    private $basketService;

    public function __construct(App\FrontModule\Model\BasketService $basketService)
    {
        parent::__construct();
        $this->basketService = $basketService;
    }

    protected function createTemplate()
    {
        $template = parent::createTemplate();
        $template->count = $this->basketService->count();
        $template->price = $this->basketService->price();
        return $template;
    }

    public function handleClear()
    {
        $this->basketService->clear();
        $this->presenter->flashMessage('Košík byl úspěšně vymazán', TRUE);
        $this->redirect('this');
    }

}
