<?php

namespace App\FrontModule\Control;

use Nette,
    App;

class BasketProductsControl extends App\Components\BaseControl
{

    /** @var App\FrontModule\Model\BasketService */
    private $basketService;

    /** @var App\FrontModule\Control\Factory\IBasketSendControl */
    public $componentsBasketSend;

    public function __construct(App\FrontModule\Model\BasketService $basketService, App\FrontModule\Control\Factory\IBasketSendControl $sendBasket)
    {
        parent::__construct();
        $this->basketService = $basketService;
        $this->componentsBasketSend = $sendBasket;
    }

    protected function createTemplate()
    {
        $template = parent::createTemplate();
        $template->products = $this->basketService->getProductsInBasket();
        $template->count = $this->basketService->count();
        $template->price = $this->basketService->price();
        return $template;
    }

    public function handleDelete($id)
    {
        $this->basketService->remove($id);
        $this->presenter->flashMessage('Produkt byl úspěšně odstraněn', TRUE);
        $this->redirect('this');
    }

    protected function createComponentUpdateCountForm()
    {
        return new Nette\Application\UI\Multiplier(function ($itemId) {
            $form = new Nette\Application\UI\Form;
            $form->addText('count')
                    ->addRule($form::FILLED, 'Množství musí být vyplněno')
                    ->addRule($form::INTEGER, 'Množství musí být vyplněno')
                    ->addRule($form::MIN, 'Minimální množství je 1', 1);


            $form->addHidden('id', $itemId);
            $form->addSubmit('send', 'Upravit');

            $form->onSuccess[] = $this->onSuccess;
            return $form;
        });
    }

    public function onSuccess(Nette\Application\UI\Form $form)
    {
        $values = $form->values;
        $this->basketService->update($values->id, $values->count);
        $this->presenter->flashMessage('Množství bylo úspěšně upraveno', TRUE);
        $this->redirect('this');
    }

}
