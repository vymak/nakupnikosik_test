<?php

namespace App\FrontModule\Control\Factory;

use App;

interface IBasketStatusControl
{

    /** @return App\FrontModule\Control\BasketStatusControl */
    function create();
}
