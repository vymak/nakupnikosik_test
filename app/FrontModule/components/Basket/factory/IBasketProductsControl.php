<?php

namespace App\FrontModule\Control\Factory;

use App;

interface IBasketProductsControl
{

    /** @return App\FrontModule\Control\BasketProductsControl */
    function create();
}
