<?php

namespace App\FrontModule\Control\Factory;

use App;

interface IBasketSendControl
{

    /** @return App\FrontModule\Control\BasketSendControl */
    function create();
}
