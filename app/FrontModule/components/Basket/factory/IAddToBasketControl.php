<?php

namespace App\FrontModule\Control\Factory;

use App;

interface IAddToBasketControl
{

    /** @return App\FrontModule\Control\AddToBasketControl */
    function create();
}
