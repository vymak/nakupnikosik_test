<?php

namespace App\FrontModule\Control;

use App,
    Nette;

class BasketSendControl extends App\Components\BaseControl
{

    /** @var App\FrontModule\Model\BasketRepository */
    private $basketRepository;

    /** @var App\FrontModule\Model\BasketService */
    private $basketService;

    public function __construct(App\FrontModule\Model\BasketRepository $basketRepository, App\FrontModule\Model\BasketService $basketService)
    {
        parent::__construct();
        $this->basketRepository = $basketRepository;
        $this->basketService = $basketService;
    }

    protected function createComponentBasketSendForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->addText('firstName', 'Jméno')
                ->addRule($form::FILLED, 'Jméno musí být zadáno');
        $form->addText('lastName', 'Příjmení')
                ->addRule($form::FILLED, 'Příjmení musí být zadáno');
        $form->addText('email', 'Email')
                ->addRule($form::FILLED, 'Email musí být zadán')
                ->addRule($form::EMAIL, 'Emailová adresa nemá správný tvar');
        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = $this->onSuccess;
        return $form;
    }

    public function onSuccess(Nette\Application\UI\Form $form)
    {
        $values = $form->values;
        $this->basketRepository->insert($this->basketService->getProductsInBasket(), $values);
        $this->basketService->clear();

        $this->presenter->flashMessage('Košík byl úspěšně uložen', TRUE);
        $this->redirect('this');
    }

}
