<?php

namespace App\FrontModule\Control;

use Nette,
    App;

/**
 * @multiControl
 */
class AddToBasketControl extends App\Components\BaseControl
{

    private $id;
    private $basketService;

    public function __construct(App\FrontModule\Model\BasketService $basketService)
    {
        parent::__construct();
        $this->basketService = $basketService;
    }

    protected function createTemplate()
    {
        $this->id = (int) $this->name;
        return parent::createTemplate();
    }

    protected function createComponentAddToBasketForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->addText('count', 'Počet kusů:')
                ->addRule($form::FILLED, 'Počet kusů je povinný')
                ->addRule($form::INTEGER, 'Počet kusů musí být celé číslo')
                ->addRule($form::MIN, 'Minimální množství je 1', 1);
        $form->addHidden('itemId', $this->id);
        $form->addSubmit('send', 'Přidat do košíku');

        $form->onSuccess[] = $this->onSuccess;
        return $form;
    }

    public function onSuccess(Nette\Application\UI\Form $form)
    {
        $values = $form->values;
        $this->basketService->add($values->itemId, $values->count);
        $this->presenter->flashMessage('Produkt byl úspěšně vložen do košíku', TRUE);
        $this->redirect('this');
    }

}
