<?php

namespace App\FrontModule\Model;

use Nette,
    Vymakdevel;

class ProductRepository extends Vymakdevel\NetteBase\Model\Model
{

    public function __construct(Nette\Database\Context $db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        return $this->db->table('Product');
    }

    /**
     * @param int|array $id
     */
    public function getById($id)
    {
        return $this->getAll()
                        ->where('id', $id);
    }

}
