<?php

namespace App\FrontModule\Model;

use Nette,
    Vymakdevel,
    App,
    stdClass;

class BasketService extends Vymakdevel\NetteBase\Model\Model
{

    /** @var Nette\Http\SessionSection */
    private $sessionSection;

    /** @var App\FrontModule\Model\ProductRepository */
    private $productRepository;

    public function __construct(Nette\Http\Session $session, App\FrontModule\Model\ProductRepository $productRep)
    {
        $this->sessionSection = $session->getSection('basket');
        $this->productRepository = $productRep;
        $this->init();
    }

    private function init()
    {
        if (!isset($this->sessionSection->basket)) {
            $this->sessionSection->basket = [];
        }
    }

    public function add($productId, $count)
    {
        if (array_key_exists($productId, $this->sessionSection->basket)) {
            $this->sessionSection->basket[$productId] = $this->sessionSection->basket[$productId] + $count;
        } else {
            $this->sessionSection->basket[$productId] = $count;
        }
    }

    public function update($productId, $count)
    {
        $this->sessionSection->basket[$productId] = $count;
    }

    public function remove($productId)
    {
        if (array_key_exists($productId, $this->sessionSection->basket)) {
            unset($this->sessionSection->basket[$productId]);
        }
    }

    public function clear()
    {
        $this->sessionSection->basket = [];
    }

    public function count()
    {
        return array_sum($this->sessionSection->basket);
    }

    public function price()
    {
        $products = $this->productRepository->getById(array_keys($this->sessionSection->basket));

        $price = 0;
        foreach ($this->sessionSection->basket as $itemId => $count) {
            $price += $products[$itemId]->price * $count;
        }
        return $price;
    }

    public function getProductsInBasket()
    {
        $products = $this->productRepository->getById(array_keys($this->sessionSection->basket));

        $arr = [];
        foreach ($this->sessionSection->basket as $itemId => $count) {
            $obj = new stdClass;
            $obj->id = $itemId;
            $obj->name = $products[$itemId]->name;
            $obj->productPrice = $products[$itemId]->price;
            $obj->count = $count;
            $obj->price = $products[$itemId]->price * $count;

            $arr[] = $obj;
        }
        return $arr;
    }

}
