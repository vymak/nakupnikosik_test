<?php

namespace App\FrontModule\Model;

use Vymakdevel,
    Nette,
    Exception,
    Tracy;

class BasketRepository extends Vymakdevel\NetteBase\Model\Model
{

    public function __construct(Nette\Database\Context $db)
    {
        $this->db = $db;
    }

    public function insert(array $products, $values)
    {
        try {
            $userId = $this->insertUser($values);
            $this->insertItems($products, $userId);
        } catch (Exception $ex) {
            Tracy\Debugger::log($ex->getMessage());
        }
    }

    private function insertItems(array $items, $userId)
    {
        foreach ($items as $value) {
            $this->db->table('BasketItems')
                    ->insert([
                        'userId' => $userId,
                        'productId' => $value->id,
                        'count' => $value->count
            ]);
        }
    }

    private function insertUser($values)
    {
        $row = $this->db->table('BasketUser')
                ->insert($values);
        return $row->getPrimary();
    }

}
