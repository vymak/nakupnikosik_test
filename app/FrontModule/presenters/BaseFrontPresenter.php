<?php

namespace App\FrontModule\Presenters;

use App;

abstract class BaseFrontPresenter extends App\Presenters\BasePresenter
{

    /** @var App\FrontModule\Control\Factory\IBasketStatusControl @inject */
    public $componentsBasketStatus;

}
