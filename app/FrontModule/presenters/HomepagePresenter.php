<?php

namespace App\FrontModule\Presenters;

use App;

class HomepagePresenter extends BaseFrontPresenter
{

    /** @var App\FrontModule\Control\Factory\IProductGridControl @inject */
    public $componentsProductGrid;

}
