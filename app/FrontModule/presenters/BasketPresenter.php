<?php

namespace App\FrontModule\Presenters;

use App;

class BasketPresenter extends BaseFrontPresenter
{

    /** @var App\FrontModule\Control\Factory\IBasketProductsControl @inject */
    public $componentsBasketProducts;

}
