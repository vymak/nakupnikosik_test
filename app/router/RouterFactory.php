<?php

namespace App;

use Nette,
    Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route;

/**
 * Router factory.
 */
class RouterFactory
{

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;

        // routelist pro modul admin
        // $router[] = $adminRoutes = new RouteList('Admin');
        // $adminRoutes[] = new Route('admin/<presenter>/<action>/[<id>/]', 'Homepage:default');
        // route list pro modul front
        $router[] = $frontRoutes = new RouteList('Front');
        $frontRoutes[] = new Route('[<locale=cs cs|en|sk>/]<presenter>/<action>/[<id>]', 'Homepage:default');
        return $router;
    }

}
