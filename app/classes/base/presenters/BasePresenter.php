<?php

namespace App\Presenters;

use Nette,
    Tracy,
    WebLoader,
    Vymakdevel;

/**
 * Description of BasePresenter
 *
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    const PUBLIC_AVAILIBLE = TRUE;

    use Vymakdevel\NetteBase\Traits\TBaseControlPresenter;

    protected function startup()
    {
        parent::startup();
        if ((Tracy\Debugger::$productionMode && !self::PUBLIC_AVAILIBLE) || (file_exists(__DIR__ . '/../../../../.htdeployment.running'))) {
            $this->template->setFile(__DIR__ . '/../../../presenters/templates/Error/maintenance.latte');
        }
    }

    public function afterRender()
    {
        if ($this->ajax && $this->hasFlashSession()) {
            $this->redrawControl('flashes');
        }
    }

}
