<?php

namespace App\Presenters;

use Nette,
    Tracy,
    Exception;

/**
 * Base Error presenter
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
class ErrorPresenter extends Nette\Application\UI\Presenter
{

    public function renderDefault(Exception $exception, $request)
    {
        if ($request) {
            if (Nette\Utils\Strings::startsWith($request->presenterName, 'Front:')) {
                $this->forward(':Front:Error:', [
                    'exception' => $exception,
                    'request' => $request,
                ]);
            } elseif (Nette\Utils\Strings::startsWith($request->presenterName, 'Cron:')) {
                $this->forward(':Cron:Error:', [
                    'exception' => $exception,
                    'request' => $request,
                ]);
            } elseif (Nette\Utils\Strings::startsWith($request->presenterName, 'Admin:')) {
                $this->forward(':Admin:Error:', [
                    'exception' => $exception,
                    'request' => $request,
                ]);
            }
        }
        $this->errorFunc($exception);
    }

    private function errorFunc(Exception $exception)
    {
        if ($this->isAjax()) { // AJAX request? Just note this error in payload.
            $this->payload->error = TRUE;
            $this->terminate();
        } elseif ($exception instanceof Nette\Application\BadRequestException) {
            $code = $exception->getCode();
            // load template 403.latte or 404.latte or ... 4xx.latte
            $this->setView(in_array($code, array(403, 404, 405, 410, 500)) ? $code : '4xx');
            // log to access.log
            Tracy\Debugger::log("HTTP code $code: {$exception->getMessage()} in {$exception->getFile()}:{$exception->getLine()}", 'access');
        } else {
            $this->setView('500'); // load template 500.latte
            Tracy\Debugger::log($exception, Tracy\Debugger::ERROR); // and log exception
        }
    }

}
