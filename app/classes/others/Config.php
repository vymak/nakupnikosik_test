<?php

namespace App\Classes\Others;

use Nette;

/**
 * Config class return parameters from parameters.neon file
 */
class Config extends Nette\Object
{

    /** @var array */
    private $parameters;

    public function __construct(Nette\DI\Container $container)
    {
        $this->parameters = $container->getParameters();
    }

}
