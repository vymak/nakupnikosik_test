<?php

namespace App\Classes\Others;

use Nette;

/**
 * Helper class
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
class Helpers extends Nette\Object
{

    /**
     * Function which register helpers to template
     * @param mixed $filter
     * @param mixed $value
     * @return mixed
     * @static
     */
    static public function common($filter, $value)
    {
        if (method_exists(__CLASS__, $filter)) {
            $args = func_get_args();
            array_shift($args);
            return call_user_func_array(array(__CLASS__, $filter), $args);
        }
    }

    /**
     * Return time ago in word
     * @param mixed $time
     * @return boolean|string
     * @author David Grudl
     * @static
     */
    static public function timeAgoInWords($time)
    {
        if (!$time) {
            return FALSE;
        } elseif (is_numeric($time)) {
            $time = (int) $time;
        } elseif ($time instanceof Nette\Utils\DateTime) {
            $time = $time->format('U');
        } else {
            $time = strtotime($time);
        }

        $delta = time() - $time;

        if ($delta < 0) {
            $delta = round(abs($delta) / 60);
            if ($delta == 0) {
                return 'za okamžik';
            }
            if ($delta == 1) {
                return 'za minutu';
            }
            if ($delta < 45) {
                return 'za ' . $delta . ' ' . self::plural($delta, 'minuta', 'minuty', 'minut');
            }
            if ($delta < 90) {
                return 'za hodinu';
            }
            if ($delta < 1440) {
                return 'za ' . round($delta / 60) . ' ' . self::plural(round($delta / 60), 'hodina', 'hodiny', 'hodin');
            }
            if ($delta < 2880) {
                return 'zítra';
            }
            if ($delta < 43200) {
                return 'za ' . round($delta / 1440) . ' ' . self::plural(round($delta / 1440), 'den', 'dny', 'dní');
            }
            if ($delta < 86400) {
                return 'za měsíc';
            }
            if ($delta < 525960) {
                return 'za ' . round($delta / 43200) . ' ' . self::plural(round($delta / 43200), 'měsíc', 'měsíce', 'měsíců');
            }
            if ($delta < 1051920) {
                return 'za rok';
            }
            return 'za ' . round($delta / 525960) . ' ' . self::plural(round($delta / 525960), 'rok', 'roky', 'let');
        }

        $delta = round($delta / 60);
        if ($delta == 0) {
            return 'před okamžikem';
        }
        if ($delta == 1) {
            return 'před minutou';
        }
        if ($delta < 45) {
            return "před $delta minutami";
        }
        if ($delta < 90) {
            return 'před hodinou';
        }
        if ($delta < 1440) {
            return 'před ' . round($delta / 60) . ' hodinami';
        }
        if ($delta < 2880) {
            return 'včera';
        }
        if ($delta < 43200) {
            return 'před ' . round($delta / 1440) . ' dny';
        }
        if ($delta < 86400) {
            return 'před měsícem';
        }
        if ($delta < 525960) {
            return 'před ' . round($delta / 43200) . ' měsíci';
        }
        if ($delta < 1051920) {
            return 'před rokem';
        }
        return 'před ' . round($delta / 525960) . ' lety';
    }

    /**
     * Plural: three forms, special cases for 1 and 2, 3, 4.
     * (Slavic family: Slovak, Czech)
     * @param $n int
     * @return mixed
     * @author David Grudl
     * @static
     */
    static private function plural($n)
    {
        $args = func_get_args();
        return $args[($n == 1) ? 1 : (($n >= 2 && $n <= 4) ? 2 : 3)];
    }

    /**
     * Smarty {mailto} function plugin
     *
     * @link http://www.smarty.net/manual/en/language.function.mailto.php {mailto}
     * (Smarty online manual)
     * @version 1.2
     * @author Monte Ohrt <monte at ohrt dot com>
     * @author credits to Jason Sweat (added cc, bcc and subject functionality)
     * @static
     */
    static public function emailProtection($email)
    {
        $address = $email;

        $match = [];
        preg_match('!^(.*)(\?.*)$!', $address, $match);
        if (!empty($match[2])) {
            trigger_error("mailto: hex encoding does not work with extra attributes. Try javascript.", E_USER_WARNING);
            return;
        }
        $addressEncode = '';
        for ($x = 0, $_length = strlen($address); $x < $_length; $x++) {
            if (preg_match('!\w!' . 'u', $address[$x])) {
                $addressEncode .= '%' . bin2hex($address[$x]);
            } else {
                $addressEncode .= $address[$x];
            }
        }
        $mailto = "&#109;&#97;&#105;&#108;&#116;&#111;&#58;";

        return '<a href="' . $mailto . $addressEncode . '">' . $address . '</a>';
    }

}
