<?php

namespace App\Classes\Filter;

use Nette;

/**
 * Minify CSS code
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
class CssMinFilter extends Nette\Object
{

    public function __invoke($code)
    {
        return \CssMin::minify($code);
    }

}
