<?php

namespace App\Classes\Filter;

use Nette,
    Tracy;

/**
 * Minify JS file
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
class JsMinFilter extends Nette\Object
{

    public function __invoke($code)
    {
        if (extension_loaded('curl')) {
            $ch = curl_init('http://closure-compiler.appspot.com/compile');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'output_format=json&output_info=compiled_code&output_info=warnings&output_info=errors&output_info=statistics&compilation_level=SIMPLE_OPTIMIZATIONS&warning_level=default&output_file_name=default.js&js_code=' . urlencode($code));
            $output = curl_exec($ch);
            curl_close($ch);

            if ($output !== FALSE) {
                $json = Nette\Utils\Json::decode($output);
                if (isset($json->errors)) {
                    foreach ($json->errors as $value) {
                        Tracy\Debugger::log($value->error . ' Line: ' . $value->line, Tracy\Debugger::WARNING);
                    }
                    return $code;
                }
                return $json->compiledCode;
            }
        }
        return $code;
    }

}
