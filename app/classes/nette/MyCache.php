<?php

namespace App\Classes\Nette;

use Nette;

/**
 * Overrided cache class from Nette framework
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
class MyCache extends Nette\Caching\Cache
{

    /**
     * Load from cache, if offset not exist insert it to cache and then return
     * @param string $name
     * @param callback $fallback
     * @param mixed $parameters
     * @return mixed
     */
    public function load($name, $fallback = NULL, $parameters = NULL)
    {
        $key = self::getNamesForCache($name, $parameters);
        return parent::load($key, $fallback);
    }

    /**
     * Save data to cache
     * @param string $name
     * @param string $data
     * @param array $dependencies
     * @param mixed $parameters
     * @return mixed
     */
    public function save($name, $data, array $dependencies = NULL, $parameters = NULL)
    {
        $key = self::getNamesForCache($name, $parameters);
        return parent::save($key, $data, $dependencies);
    }

    /**
     * Cleans cache by tags
     * @param string|array $tags
     */
    public final function cleanByTags($tags = NULL)
    {
        if (!isset($tags)) {
            $this->clean([
                MyCache::ALL => TRUE
            ]);
        } else {
            $this->clean([
                MyCache::TAGS => (is_array($tags)) ? $tags : [$tags]
            ]);
        }
    }

    /**
     * Clean cache by priority
     * @param int $priority
     */
    public final function cleanByPriority($priority)
    {
        if (!Nette\Utils\Validators::isNumericInt($priority)) {
            throw new Nette\InvalidArgumentException;
        }

        $this->clean([
            MyCache::PRIORITY => $priority
        ]);
    }

    /**
     * Generate unique cache name by attributes
     * @param string $methodname
     * @param array|string $parameters
     * @return string
     * @static
     */
    static public function getNamesForCache($methodname, $parameters = NULL)
    {
        if (empty($parameters)) {
            return $methodname;
        }

        if (is_array($parameters)) {
            foreach ($parameters as $value) {
                if (!empty($value)) {
                    if (is_array($value)) {
                        $methodname .= '_' . implode('_', $value);
                    } else {
                        $methodname .= '_' . $value;
                    }
                }
            }
        } else {
            $methodname .= '_' . $parameters;
        }
        return $methodname;
    }

}
